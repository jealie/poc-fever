class Agent:

    def __init__(self, model: str, platform:str="groq", prompt_history: bool = True):
        self.platform = platform
        if platform == "groq":
            from groq import Groq

            self.client = Groq(
                api_key="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"
            )
        else:
            raise RuntimeError(f"platform {platform} not implemented")
        self.messages = []
        self.model = model
        self.prompt_history = prompt_history

    def set_system(self, prompt: str):
        """
        Sets the system prompt

        Parameters
        ----------
        prompt : str
            The system prompt for the LLM

        """
        self.messages.append({"role": "system", "content": prompt})

    def chat(self, prompt: str, return_as_json: bool = False):
        """
        One line in a dialog with a LLM.

        Parameters
        ----------
        prompt : str
            The prompt for the LLM
        return_as_json : book
            whether of not to return as a json-parsable object (works only with OpenAI, not Groq)

        Returns
        -------
        str
            The LLM answer

        """
        if return_as_json:
            if self.platform == "groq":
                raise RuntimeError("Groq does not handle the `json_object` option")
            chat_completion = self.client.chat.completions.create(
                messages=self.messages + [{"role": "user", "content": prompt}],
                model=self.model,
                response_format={"type": "json_object"},
            )
        else:
            chat_completion = self.client.chat.completions.create(
                messages=self.messages + [{"role": "user", "content": prompt}],
                model=self.model,
            )
        response = chat_completion.choices[0].message.content
        if self.prompt_history:
            self.messages.append({"role": "system", "content": response})
        return response


