import json

from langchain_chroma import Chroma
from langchain_community.embeddings.sentence_transformer import (
    SentenceTransformerEmbeddings,
)
from tqdm.auto import tqdm

from fever.agent import Agent

def add_retrieved_data(query, db):
    queried_docs = db.similarity_search(query, k=5)
    return "\n===\n" + "\n===\n".join([q.page_content for q in queried_docs]) + "\n==="

embedding_function = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")
db = Chroma(
    persist_directory="data/chromadb", embedding_function=embedding_function
)

agent = Agent(model="mixtral-8x7b-32768", platform="groq", prompt_history=False)
agent.set_system(
    """Your name is Jean-Henri, a smart fact-checker having access to top-k closest hits retrieved from an existing database that is the ground truth. Your responses are written in JSON format and have to be one of three options: {"label": "SUPPORTS"} if the top-k closest hits support the given claim, or {"label": "REFUTES"} if the top-k closest hits refute the given claim, or  {"label": "NOT ENOUGH INFO"} if it is impossible to conclude based on the top-k closest hits."""
)

claims = json.load(open('../data/sample_claims.json', 'r'))

stats = {"agrees": 0, "disagrees": 0, "chicken": 0}

for claim in tqdm(claims):
    claim_test = claim['claim']
    label = claim['label']
    enriched_prompt = (
        f"\nHere are the top closest hits for the query '''{claim_test}''':\n{add_retrieved_data(claim_test, db)}.\n\nNow, answer the question:\n\n"
    )
    answer = agent.chat(enriched_prompt)
    predicted_label = "NOT ENOUGH INFO"
    try:
        answer = answer[answer.find('{'):]
        answer = answer[:answer.find('}')+1]
        answer = answer.replace('\n', '').replace('\r', '').replace('\t', '')
        predicted_label = json.loads(answer).get("label", "NOT ENOUGH INFO")
    except:
        print(f"could not parse LLM output {answer}")
    if predicted_label == label:
        stats["agrees"] += 1
        print(f"ALL GOOD: claim = {claim_test}, both answers are {label}")
    elif predicted_label == "NOT ENOUGH INFO":
        stats["chicken"] += 1
        print(f"chicken: claim = {claim_test}, should have answered {label}")
    else:
        stats["disagrees"] += 1
        print(f"booh: claim = {claim_test}, {label=} but {predicted_label=}")

print(stats)
