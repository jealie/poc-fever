import json
import pywikibot
import wikitextparser

from tqdm.auto import tqdm

DO_TOP_N = 100 # roughly 30 minutes


train = open('train.jsonl')

site = pywikibot.Site('wikipedia:en')

wikipedia_pages = {}
claims = []

for l in tqdm(train.readlines()[:DO_TOP_N]):
    d = json.loads(l)
    print(d.get("claim", ""))
    try:
        to_add = {}
        evidences = d.get('evidence', [[]])
        for e in evidences[0]:
            p = e[2]
            if p not in wikipedia_pages:
                page = pywikibot.Page(site, p)
                chosen_rev = None
                for r in tqdm(page.revisions(content=False)):
                    if f"{r['timestamp'].year}-{r['timestamp'].month:02}" < '2017-06':
                        break
                    else:
                        chosen_rev = r["revid"]
                if chosen_rev is None:
                    raise RuntimeError
                old_page = page.getOldVersion(oldid=r['revid'])
                parsed = wikitextparser.parse(old_page)
                to_add[p] = parsed.plain_text()
        claims.append(d)
        wikipedia_pages.update(to_add)
    except:
        print(f'FAILED at {d}')

import ipdb; ipdb.set_trace()
