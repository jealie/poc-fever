from langchain_chroma import Chroma
from langchain_community.document_loaders import TextLoader
from langchain_community.embeddings.sentence_transformer import SentenceTransformerEmbeddings
from langchain_text_splitters import CharacterTextSplitter
from langchain_core.documents.base import Document

import json

docs = []

for page_name, page_content in json.load(open('../data/sample_sources.json', 'r')).items():
    docs.append(
        Document(
            page_content=page_content,
            metadata={"page_name": page_name},
        )
    )

text_splitter = CharacterTextSplitter(separator='.', chunk_size=1000, chunk_overlap=0)

docs = text_splitter.split_documents(docs)

embedding_function = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")

db = Chroma.from_documents(documents=docs, embedding=embedding_function, persist_directory='../data/chromadb')

query = "When was born Nikolaj Coster-Waldau?"
queried_docs = db.similarity_search(query, k=5)
print(queried_docs)
