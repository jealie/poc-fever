import streamlit as st

from langchain_chroma import Chroma
from langchain_community.embeddings.sentence_transformer import (
    SentenceTransformerEmbeddings,
)

from fever.agent import Agent

"""
# POC 💡 for RAG 😡 on FEVER 🌡️

Type in the box below to chat with the bot:
"""

# Initialize chat history
if "messages" not in st.session_state:
    st.session_state.messages = []

# Initialize agents
if "RAG_agent" not in st.session_state:
    # This agent performs the RAG over the dataset
    # This is the only agent with access to its prompt history
    st.session_state.RAG_agent = Agent(model="mixtral-8x7b-32768", platform="groq", prompt_history=False)
    st.session_state.RAG_agent.set_system(
        """Your name is Jean-Henri, a smart fact-checker having access to top-k closest hits retrieved from an existing database that is the ground truth. Your responses are written in JSON format and have to be one of three options: {"label": "SUPPORTS"} if the top-k closest hits support the given claim, or {"label": "REFUTES"} if the top-k closest hits refute the given claim, or  {"label": "NOT ENOUGH INFO"} if it is impossible to conclude based on the top-k closest hits."""
    )

# Initialize DB
if "db" not in st.session_state:
    # Loads the chroma db embeddings
    embedding_function = SentenceTransformerEmbeddings(model_name="all-MiniLM-L6-v2")
    st.session_state.db = Chroma(
        persist_directory="data/chromadb", embedding_function=embedding_function
    )


def add_retrieved_data(query, db):
    queried_docs = db.similarity_search(query, k=5)
    return "\n===\n".join([q.page_content for q in queried_docs]) + "\n==="


# Display chat messages and graphs from history on app rerun
for message in st.session_state.messages:
    with st.chat_message(message["role"]):
        st.markdown(message["content"])

# React to user input
if prompt := st.chat_input("Try me... but not too hard! I am very experimental."):
    # Display user message in chat message container
    with st.chat_message("user"):
        st.markdown(prompt)
    # Add user message to chat history
    st.session_state.messages.append({"role": "user", "content": prompt})

# Display assistant response in chat message container
if prompt is not None:
    print(f"{prompt=}")
    with st.chat_message("assistant"):
        enriched_prompt = (
            f"\nHere are the top closest hits for the query '''{prompt}''':\n{add_retrieved_data(prompt, st.session_state.db)}.\n\nNow, answer the question:\n\n"
        )
        print(f"{enriched_prompt=}")
        response = st.session_state.RAG_agent.chat(
            enriched_prompt, return_as_json=False
        )
        print(f"{response=}")
        st.markdown(response)
        st.session_state.messages.append({"role": "assistant", "content": response})
