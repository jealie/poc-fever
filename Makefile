help: ## Display this help
	@egrep -h '\s##\s' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m  %-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the docker
	docker build --build-arg USER_NAME=$(USER) --build-arg GROUP_NAME=$$(id -gn $(USER)) --build-arg USER_ID=$$(id -u $(USER)) --build-arg GROUP_ID=$$(id -g $(USER)) -t fever .

run-bash: ## Run a bash session with the docker
	docker run --user=$$(id -u $(USER)):$$(id -g $(USER)) -v $$(pwd):/workspace/ -p 8500:8500 -it fever bash

run-streamlit: ## Run a streamlit with the docker
	docker run --network=host --user=$$(id -u $(USER)):$$(id -g $(USER)) -v $$(pwd):/workspace/ -p 8501:8501 -it fever streamlit run experimental_chat.py --server.port=8501 --server.address=0.0.0.0

test: ## Run the test suite
	docker run --user=$$(id -u $(USER)):$$(id -g $(USER)) -v $$(pwd):/workspace/ -it fever pytest tests/agent.py
