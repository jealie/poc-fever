# POC 💡 for RAG 😡 on FEVER 🌡️

Find here:

- the documented code as a tested package (`fever` & `tests` directories)

- a Streamlit interface (`experimental_chat.pdf`)

## Repo organization


```
├── data                                  # => The data used (json)
├── fever                                 # => Routines for calling a LLM agent
│   └── agent.py
├── scripts
│   ├── create_db.py                      # => creates the chroma db
│   ├── fever_check.py                    # => performs a summary check on 66 sample claims
│   └── fever_retrieval.py                # => parses the big 
├── tests
│   └── agent.py
├── experimental_chat.py                  # => Streamlit Chat Interface
├── Dockerfile/Makefile/setup.py          # => for docker/package
└── readme.md                             # => This file
```


## Managing the docker with the Makefile

Type `make` to display the list of possible options:

```
  help                           Display this help
  build                          Build the docker
  run-bash                       Run a bash session with the docker
  run-streamlit                  Run a streamlit with the docker
  test                           Run the test suite
```

The streamlit interface can be accessed at http://localhost:8051
