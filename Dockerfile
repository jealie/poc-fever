FROM python:3.10

# Work inside the docker using the same user as outside the docker
# (useful to maintain file persmissions of mounted directory)
ARG USER_NAME
ARG GROUP_NAME
ARG USER_ID
ARG GROUP_ID
RUN addgroup --gid $GROUP_ID $GROUP_NAME && adduser --disabled-password --gecos '' --uid $USER_ID --gid $GROUP_ID $USER_NAME
WORKDIR /workspace
RUN chown $USER_NAME:$GROUP_NAME /workspace

# System-wide conf
RUN pip3 install --upgrade pip
COPY requirements.python ./
RUN pip3 install --no-cache-dir -r requirements.python

# Now switching to local user with no special permission
USER $USER_NAME

# Install the package in edit (-e) mode
COPY fever /workspace/
COPY setup.py /workspace/
RUN cd /workspace && pip3 install -e .
